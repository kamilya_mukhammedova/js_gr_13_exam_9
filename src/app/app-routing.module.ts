import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CocktailsComponent } from './cocktails/cocktails.component';
import { NewCocktailComponent } from './new-cocktail/new-cocktail.component';

const routes: Routes = [
  {path: '', component: CocktailsComponent},
  {path: 'add-new-cocktail', component: NewCocktailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
