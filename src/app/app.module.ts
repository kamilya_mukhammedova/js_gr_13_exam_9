import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { CocktailsComponent } from './cocktails/cocktails.component';
import { NewCocktailComponent } from './new-cocktail/new-cocktail.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CocktailService } from './shared/cocktail.service';
import { ValidateImageDirective } from './new-cocktail/validate-image.directive';
import { ModalComponent } from './ui/modal/modal.component';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    CocktailsComponent,
    NewCocktailComponent,
    ValidateImageDirective,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [CocktailService],
  bootstrap: [AppComponent]
})
export class AppModule { }
