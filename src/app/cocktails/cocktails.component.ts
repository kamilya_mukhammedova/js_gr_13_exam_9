import { Component, OnDestroy, OnInit } from '@angular/core';
import { CocktailService } from '../shared/cocktail.service';
import { Cocktail } from '../shared/cocktail.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-cocktails',
  templateUrl: './cocktails.component.html',
  styleUrls: ['./cocktails.component.css']
})
export class CocktailsComponent implements OnInit, OnDestroy {
  cocktails!: Cocktail[];
  cocktailsChangeSubscription!: Subscription;
  cocktailsFetchingSubscription!: Subscription;
  isFetching = false;
  modalOpen = false;
  currentCocktail!: Cocktail | null;

  constructor(private cocktailService: CocktailService) {}

  ngOnInit(): void {
    this.cocktailsChangeSubscription = this.cocktailService.cocktailsChange.subscribe((cocktails: Cocktail[]) => {
      this.cocktails = cocktails;
    });
    this.cocktailsFetchingSubscription = this.cocktailService.cocktailsFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.cocktailService.fetchCocktails();
  }

  openModalWindow(cocktail: Cocktail) {
    this.modalOpen = true;
    this.currentCocktail = cocktail;
  }

  closeModalWindow() {
    this.modalOpen = false;
  }

  ngOnDestroy(): void {
    this.cocktailsChangeSubscription.unsubscribe();
    this.cocktailsFetchingSubscription.unsubscribe();
  }
}
