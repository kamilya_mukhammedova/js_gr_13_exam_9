import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { CocktailService } from '../shared/cocktail.service';
import { imageValidator } from './validate-image.directive';
import { Cocktail } from '../shared/cocktail.model';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-new-cocktail',
  templateUrl: './new-cocktail.component.html',
  styleUrls: ['./new-cocktail.component.css']
})
export class NewCocktailComponent implements OnInit, OnDestroy {
  cocktailForm!: FormGroup;
  ingredientBtnIsClicked = false;
  cocktailUploadingSubscription!: Subscription;
  isUploading = false;

  constructor(private cocktailService: CocktailService, private router: Router) { }

  ngOnInit(): void {
    this.cocktailForm = new FormGroup({
      name: new FormControl('', Validators.required),
      image: new FormControl('', [Validators.required, imageValidator]),
      type: new FormControl('', Validators.required),
      description: new FormControl(''),
      ingredients: new FormArray([], Validators.required),
      method: new FormControl('', Validators.required)
    });
    this.cocktailUploadingSubscription = this.cocktailService.cocktailUploading.subscribe((isUploading: boolean) => {
      this.isUploading = isUploading;
    });
  }

  addIngredient() {
    const ingredients = <FormArray>this.cocktailForm.get('ingredients');
    const ingredientsGroup = new FormGroup({
      ingredientName: new FormControl('', Validators.required),
      quantity: new FormControl('', Validators.required),
      measurement: new FormControl('', Validators.required),
    });
    ingredients.push(ingredientsGroup);
    this.ingredientBtnIsClicked = true;
  }

  getIngredientsControls() {
    const ingredients = <FormArray>this.cocktailForm.get('ingredients');
    return ingredients.controls;
  }

  formFieldHasError(fieldName: string, errorType: string) {
    const field = this.cocktailForm.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  ingredientsControlHasError(fieldName: string, index: number, errorType: string) {
    const ingredients = <FormArray>this.cocktailForm.get('ingredients');
    const ingredientsArray = <FormGroup>ingredients.controls[index];
    const ingredientsControl = ingredientsArray.get(fieldName);
    return Boolean(ingredientsControl && ingredientsControl.touched && ingredientsControl.errors?.[errorType]);
  }

  formIsInvalid() {
    return Boolean(this.cocktailForm.invalid);
  }

  removeIngredient(index: number) {
   const ingredients = <FormArray>this.cocktailForm.get('ingredients');
   ingredients.removeAt(index);
  }

  onSubmit() {
    const id =  Math.random().toString();
    const newCocktail = new Cocktail(
      id,
      this.cocktailForm.value.name,
      this.cocktailForm.value.image,
      this.cocktailForm.value.type,
      this.cocktailForm.value.ingredients,
      this.cocktailForm.value.method,
      this.cocktailForm.value.description,
    );
   this.cocktailService.addNewCocktail(newCocktail).subscribe(() => {
     void this.router.navigate(['/']);
   });
  }

  ngOnDestroy(): void {
    this.cocktailUploadingSubscription.unsubscribe();
  }
}
