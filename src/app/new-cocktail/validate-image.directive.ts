import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
import { Directive } from '@angular/core';

export const imageValidator = (control: AbstractControl): ValidationErrors | null => {
  const http = control.value.substr(0, 7);
  const https = control.value.substr(0, 8);
  if(http === 'http://' || https === 'https://') {
    return null;
  }
  return {image: true};
}

@Directive({
  selector: '[appImage]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidateImageDirective,
    multi: true
  }]
})
export class ValidateImageDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return imageValidator(control);
  }
}
