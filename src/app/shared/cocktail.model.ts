export class Cocktail {
  constructor(
    public id: string,
    public name: string,
    public image: string,
    public type: string,
    public ingredients: {ingredientName: string, quantity: number, measurement: string}[],
    public method: string,
    public description: string,
  ) {}
}
