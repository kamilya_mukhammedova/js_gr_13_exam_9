import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cocktail } from './cocktail.model';
import { Subject } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable()
export class CocktailService {
  cocktailsChange = new Subject<Cocktail[]>();
  cocktailsFetching = new Subject<boolean>();
  cocktailUploading = new Subject<boolean>();

  private cocktailsArray: Cocktail[] = [];

  constructor(private http: HttpClient) {}

  fetchCocktails() {
    this.cocktailsFetching.next(true);
    this.http.get<{[id: string]: Cocktail}>('https://kamilya-61357-default-rtdb.firebaseio.com/cocktails.json')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const cocktail = result[id];
          return new Cocktail(
            id,
            cocktail.name,
            cocktail.image,
            cocktail.type,
            cocktail.ingredients,
            cocktail.method,
            cocktail.description
          );
        });
      }))
      .subscribe(cocktails => {
        this.cocktailsArray = cocktails;
        this.cocktailsChange.next(this.cocktailsArray.slice());
        this.cocktailsFetching.next(false);
      }, () => {
        this.cocktailsFetching.next(false);
      });
  }

  addNewCocktail(cocktail: Cocktail) {
    const body = {
      name: cocktail.name,
      image: cocktail.image,
      type: cocktail.type,
      ingredients: cocktail.ingredients,
      method: cocktail.method,
      description: cocktail.description
    };
    this.cocktailUploading.next(true);
    return this.http.post('https://kamilya-61357-default-rtdb.firebaseio.com/cocktails.json', body)
      .pipe(tap(() => {
        this.cocktailUploading.next(false);
      }, () => {
        this.cocktailUploading.next(false);
      }));
  }
}
